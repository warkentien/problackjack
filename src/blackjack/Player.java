package blackjack;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by sethf_000 on 11/14/2015.
 *
 * Player class keeps track of Player's score, money and current bet.
 */
public class Player {

    private int money;
    private int score;
    private int bet;
    private ArrayList<Integer> cardList = new ArrayList<>();

    public Player(int money){
        this.money = money;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        cardList.add(score);

        for (int i = 0; i < cardList.size(); i++) {
            System.out.println("player card list before " + cardList.get(i));
        }
        int total = 0;

        for (int i = 0; i < cardList.size(); i++) {
            total += cardList.get(i);
        }

        if (total < 21){
            //maximize the ace
            for (int i = 0; i < cardList.size(); i++) {
                if (cardList.get(i) == 1){
                    if (11 + score <= 21){
                        cardList.set(i, 11);
                    }
                }
            }
        }

        total = 0;

        for (int i = 0; i < cardList.size(); i++) {
            total += cardList.get(i);
        }

        if (total > 21){
            //minimize the ace
            for (int i = 0; i < cardList.size(); i++) {
                if (cardList.get(i) == 11){
                    if (1 + score <= 21){
                        cardList.set(i, 1);
                    }
                }
            }
        }


        total = 0;
        for (int i = 0; i < cardList.size(); i++) {
            total += cardList.get(i);
        }

        for (int i = 0; i < cardList.size(); i++) {
            System.out.println("player card list after " + cardList.get(i));
        }

        this.score = total;
    }

    public int setBet() {
        bet = 100;
        return bet;
    }

    public void doubleBet() {
        bet = bet * 2;
    }

    public int getMoney(int n) {

        if(n == 1){
            money = money + bet;
            return money;
        }
        else if(n == 2){
            money = money - bet;
            return money;
        }
        else {
            return money;
        }

    }

    public int getBet() {
        return bet;
    }

    public void resetScore() {
        cardList.clear();
    }

    public void resetBet() {
        bet = 100;
    }

}
