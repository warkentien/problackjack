package blackjack;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by sethf_000 on 11/14/2015.
 *
 * Deck class loads all the cards for the game. Shoe number can be changed with the
 * DECKS variable.
 */
public class Deck {

    private ArrayList<String> cards = new ArrayList<String>();
    private Card card = new Card();
    private final int DECKS = 6;
    private int shoe;

    public Deck(){
        loadShoe();
    }

    //loads cards from Card class in linear order, 52 at a time
    private void loadShoe() {
        shoe = 52 * DECKS;

        for (int i = 0; i < shoe - 1; i++) {
            cards.add(card.addCard());
        }

    }

    //selects a card a random from the shoe
    public String getCard(){
        Random rand = new Random();
        int index = rand.nextInt(cards.size());
        String nextCard = cards.remove(index);
        return nextCard;
    }


}
