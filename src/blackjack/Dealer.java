package blackjack;

import java.util.ArrayList;

/**
 * Created by sethf_000 on 11/14/2015.
 *
 * Dealer class keeps track of dealer's score
 */
public class Dealer {

    private int firstCard;
    private int score;
    private ArrayList<Integer> cardList = new ArrayList<>();

    public void setFirstCard(int firstCard) {
        this.firstCard = firstCard;
    }

    public int getVisibleScore() {
        return score - firstCard;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        cardList.add(score);

        for (int i = 0; i < cardList.size(); i++) {
            System.out.println("dealer card list before " + cardList.get(i));
        }

        int total = 0;
        for (int i = 0; i < cardList.size(); i++) {
            total += cardList.get(i);
        }

        if (total < 21){
            //maximize the ace
            for (int i = 0; i < cardList.size(); i++) {
                if (cardList.get(i) == 1){
                    if (11 + score <= 21){
                        cardList.set(i, 11);
                    }
                }
            }
        }

        total = 0;

        for (int i = 0; i < cardList.size(); i++) {
            total += cardList.get(i);
        }

        if (total > 21){
            //minimize the ace
            for (int i = 0; i < cardList.size(); i++) {
                if (cardList.get(i) == 11){
                    if (1 + score <= 21){
                        cardList.set(i, 1);
                    }
                }
            }
        }
       

        total = 0;
        for (int i = 0; i < cardList.size(); i++) {
            total += cardList.get(i);
        }

        for (int i = 0; i < cardList.size(); i++) {
            System.out.println("dealer card list after " + cardList.get(i));
        }

        this.score = total;
    }

    public void resetScore() {
        cardList.clear();
        firstCard = 0;
    }
}
