package blackjack;

/**
 * Created by sethf_000 on 11/14/2015.
 *
 * Driver controls the logic of the game flow. When buttons are clicked in View class,
 * the action variable in Driver gets set.
 * Dealer always hits on 15, holds on 16.
 */
public class Driver {
    public static int action;

    public static void main(String[] args) {

        View view = new View();
        BlackJack blackJack = new BlackJack(view);

        while(true){

        switch (action) {
            //deal the cards for a new game
            case 0:
                blackJack.deal();
                action = -1;
                break;

            //when the hit button is clicked, cards for both players dealt
            case 1:
                blackJack.dealerHit();
                blackJack.playerHit();
                action = -1;
                break;

            //when the stick button is clicked, no more cards dealt, scores tabulated
            case 2:
                if(blackJack.getDealerScore() < 16 ){
                    blackJack.dealerHit();
                }

                blackJack.reportScore();
                action = -1;
                break;

            //when double down button is clicked, bet is doubled
            case 3:
                blackJack.doubleBet();
                action = -1;
                break;

            //when new game button is clicked, new cards dealt
            case 4:
                blackJack.reset();
                action = 0;
                break;

            default:
                break;

            }

        }


    }

    //view sets the action after a button is clicked
    public void setAction(int n){
        action = n;
    }

}
