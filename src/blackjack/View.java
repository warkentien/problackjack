package blackjack;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * Created by sethf_000 on 11/14/2015.
 *
 * This is the GUI class, it creates all the frame and panel components. When a button is clicked,
 * the value is sent back to the driver to determine the logic of what happens next in the game
 */
public class View {

    //create frame, buttons and panels
    JFrame frame = new JFrame();
    private int FRAME_WIDTH = 600;
    private int FRAME_HEIGHT = 600;
    JButton button1 = new JButton("Hit");
    JButton button2 = new JButton("Stick");
    JButton button3 = new JButton("Double Down");
    JButton button4 = new JButton("New Game");
    JPanel panel = new JPanel();
    JPanel lowerPanel = new JPanel();
    JPanel leftPanel = new JPanel();
    Driver main = new Driver();
    ActionListener listener = new ButtonListener();
    JLabel dealerScore = new JLabel();
    JLabel playerScore = new JLabel();
    JTextArea resultArea = new JTextArea("", 8, 20);



    public View (){
        viewSetup();
    }

    //called on program start or when new game button is clicked
    public void viewSetup(){
        //set default size of frame
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);

        //add panels to top, bottom and left
        frame.getContentPane().add(panel, BorderLayout.NORTH);
        frame.getContentPane().add(lowerPanel, BorderLayout.SOUTH);
        frame.getContentPane().add(leftPanel, BorderLayout.WEST);

        button1.addActionListener(listener);
        button2.addActionListener(listener);
        button3.addActionListener(listener);
        button4.addActionListener(listener);

        //commands that switch between different button clicks
        button1.setActionCommand("1");
        button2.setActionCommand("2");
        button3.setActionCommand("3");
        button4.setActionCommand("4");

        //add the buttons and labels to panels
        panel.add(dealerScore);
        lowerPanel.add(playerScore);
        leftPanel.add(button1);
        leftPanel.add(button2);
        leftPanel.add(button3);
        leftPanel.add(button4);

        resultArea.setEditable(false);
        leftPanel.add(resultArea);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    //called whenever a new card is dealt
    public void displayCard(String card, int n, int value){

        ImageIcon icon = new ImageIcon(getClass().getResource("cards/" + card));
        Image scaleImage = icon.getImage().getScaledInstance(126, 196,Image.SCALE_DEFAULT);

        ImageIcon newIcon = new ImageIcon(scaleImage);
        JLabel label = new JLabel();
        label.setIcon(newIcon);

        //if it's dealer's card
        if (n == 1){
            panel.add(label);
            dealerScore.setText("Dealer Showing: " + value);
            panel.repaint();
        }

        //if it's player's card
        if (n == 2){
            lowerPanel.add(label);
            playerScore.setText("Player Score: " + value);
            lowerPanel.repaint();
        }

        panel.repaint();
        lowerPanel.repaint();
        resultArea.repaint();
        frame.pack();
        frame.setVisible(true);
    }

    //just to show the back of card image
    public void displayBackOfCard(){
        ImageIcon icon = new ImageIcon(getClass().getResource("cards/playing-card-back.jpg"));
        Image scaleImage = icon.getImage().getScaledInstance(126, 196,Image.SCALE_DEFAULT);

        ImageIcon newIcon = new ImageIcon(scaleImage);
        JLabel label = new JLabel();
        label.setIcon(newIcon);
        panel.add(label);
    }

    //if both players are over
    public void busts(int playerScore, int dealerScore, int money){
        resultArea.setText("");
        resultArea.append("** Player's Score: " + playerScore + "\n");
        resultArea.append("** Dealer's Score: " + dealerScore + "\n");
        resultArea.append("** DEALER AND PLAYER BUST! **\n");
        resultArea.append("** NO ONE WINS! **\n");
        resultArea.append("** Player's Money: $" + money);
    }
    //if the player wins
    public void playerWins(int playerScore, int dealerScore, int money){
        if(dealerScore > 21) {
            resultArea.setText("");
            resultArea.append("** Player's Score: " + playerScore + "\n");
            resultArea.append("** Dealer's Score: " + dealerScore + "\n");
            resultArea.append("** DEALER BUSTS! **\n");
            resultArea.append("** PLAYER WINS! **\n");
            resultArea.append("** Player's Money: $" + money);
        }
        else {
            resultArea.setText("");
            resultArea.append("** Player's Score: " + playerScore + "\n");
            resultArea.append("** Dealer's Score: " + dealerScore + "\n");
            resultArea.append("** PLAYER WINS! **\n");
            resultArea.append("** Player's Money: $" + money);
        }
    }
    //if the dealer wins
    public void dealerWins(int playerScore, int dealerScore, int money){
        if(playerScore > 21) {
            resultArea.setText("");
            resultArea.append("** Player's Score: " + playerScore + "\n");
            resultArea.append("** Dealer's Score: " + dealerScore + "\n");
            resultArea.append("** PLAYER BUSTS! **\n");
            resultArea.append("** DEALER WINS! **\n");
            resultArea.append("** Player's Money: $" + money);
        }
        else {
            resultArea.setText("");
            resultArea.append("** Player's Score: " + playerScore + "\n");
            resultArea.append("** Dealer's Score: " + dealerScore + "\n");
            resultArea.append("** DEALER WINS! **\n");
            resultArea.append("** Player's Money: $" + money);
        }
    }
    //if there is a tie
    public void tie(int playerScore, int dealerScore, int money){
        resultArea.setText("");
        resultArea.append("** Player's Score: " + playerScore + "\n");
        resultArea.append("** Dealer's Score: " + dealerScore + "\n");
        resultArea.append("** TIE! **\n");
        resultArea.append("** NO ONE WINS! **\n");
        resultArea.append("** Player's Money: $" + money);
    }

    //on new game call
    public void reset(int money){
        resultArea.setText("Player's Money: $" + money);
        panel.removeAll();
        lowerPanel.removeAll();

    }

    //show the player's bet and current money
    public void displayBet(int bet, int money){
        resultArea.setText("");
        resultArea.append("** Player's Money: $" + money + "\n");
        resultArea.append("** Player's Bet: $" + bet);
        frame.pack();
        frame.setVisible(true);
    }

    //switches based on button clicked
    class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {

            int action = Integer.parseInt(event.getActionCommand());
            //when the roll button is clicked
            switch (action) {
                //hit button
                case 1:
                    main.setAction(1);
                    break;

                //stick button
                case 2:
                    main.setAction(2);
                    break;

                //double down button
                case 3:
                    main.setAction(3);
                    break;

                case 4:
                    main.setAction(4);
                    break;

                default:
                    break;
            }

        }
    }
}
