package blackjack;

import javax.swing.*;

/**
 * Created by sethf_000 on 11/14/2015.
 *
 * Card class returns the file name for the card when it is called.
 * Deck class loads cards in linear order, 52 at a time.
 */
public class Card {
    private int count;

    public Card(){

    }

    public String addCard(){
        count++;

        if (count > 52){
            count = 1;
        }

        if (count == 1){
            return "2_of_clubs.png";
        }
        if (count == 2){
            return "2_of_diamonds.png";
        }
        if (count == 3){
            return "2_of_hearts.png";
        }
        if (count == 4){
            return "2_of_spades.png";
        }
        if (count == 5){
            return "3_of_clubs.png";
        }
        if (count == 6){
            return "3_of_diamonds.png";
        }
        if (count == 7){
            return "3_of_hearts.png";
        }
        if (count == 8){
            return "3_of_spades.png";
        }
        if (count == 9){
            return "4_of_clubs.png";
        }
        if (count == 10){
            return "4_of_diamonds.png";
        }
        if (count == 11){
            return "4_of_hearts.png";
        }
        if (count == 12){
            return "4_of_spades.png";
        }
        if (count == 13){
            return "5_of_clubs.png";
        }
        if (count == 14){
            return "5_of_diamonds.png";
        }

        if (count == 15){
            return "5_of_hearts.png";
        }
        if (count == 16){
            return "5_of_spades.png";
        }
        if (count == 17){
            return "6_of_clubs.png";
        }
        if (count == 18){
            return "6_of_diamonds.png";
        }
        if (count == 19){
            return "6_of_hearts.png";
        }
        if (count == 20){
            return "6_of_spades.png";
        }
        if (count == 21){
            return "7_of_clubs.png";
        }
        if (count == 22){
            return "7_of_diamonds.png";
        }
        if (count == 23){
            return "7_of_hearts.png";
        }
        if (count == 24){
            return "7_of_spades.png";
        }
        if (count == 25){
            return "8_of_clubs.png";
        }
        if (count == 26){
            return "8_of_diamonds.png";
        }
        if (count == 27){
            return "8_of_hearts.png";
        }
        if (count == 28){
            return "8_of_spades.png";
        }
        if (count == 29){
            return "9_of_clubs.png";
        }
        if (count == 30){
            return "9_of_diamonds.png";
        }
        if (count == 31){
            return "9_of_hearts.png";
        }
        if (count == 32){
            return "9_of_spades.png";
        }
        if (count == 33){
            return "10_of_clubs.png";
        }
        if (count == 34){
            return "10_of_diamonds.png";
        }
        if (count == 35){
            return "10_of_hearts.png";
        }
        if (count == 36){
            return "10_of_spades.png";
        }
        if (count == 37){
            return "ace_of_clubs.png";
        }
        if (count == 38){
            return "ace_of_diamonds.png";
        }
        if (count == 39){
            return "ace_of_hearts.png";
        }
        if (count == 40){
            return "ace_of_spades.png";
        }
        if (count == 41){
            return "jack_of_clubs2.png";
        }
        if (count == 42){
            return "jack_of_diamonds2.png";
        }
        if (count == 43){
            return "jack_of_hearts2.png";
        }
        if (count == 44){
            return "jack_of_spades2.png";
        }
        if (count == 45){
            return "king_of_clubs2.png";
        }
        if (count == 46){
            return "king_of_diamonds2.png";
        }
        if (count == 47){
            return "king_of_hearts2.png";
        }
        if (count == 48){
            return "king_of_spades2.png";
        }
        if (count == 49){
            return "queen_of_clubs2.png";
        }
        if (count == 50){
            return "queen_of_diamonds2.png";
        }
        if (count == 51){
            return "queen_of_hearts2.png";
        }
        if (count == 52){
            return "queen_of_spades2.png";
        }

        else return "";

    }

    public String backOfCard(){
        return "playing-card-back.jpg";
    }


}
