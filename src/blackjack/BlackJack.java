package blackjack;

import java.util.ArrayList;

/**
 * Created by sethf_000 on 11/14/2015.
 *
 * Controls all the action of the game. Driver calls methods based on button clicks
 * from View.
 */
public class BlackJack {

    //create view variable
    View view;
    //set final bet variable and value
    private static final double BET = 100.00;

    //create new objects
    private Dealer dealer = new Dealer();
    private Player player = new Player(1000);
    private Deck deck;
    //card is used to get the value of the card
    private String card;
    //value holds the point value of the card
    private int value;

    //constructor brings in view object from Driver, creates deck object
    public BlackJack(View view){
        this.view = view;
        deck = new Deck();

    }

    //called from case 0 in driver
    public void deal(){

        //displays back of card
        view.displayBackOfCard();
        //get dealer's hidden card
        card = deck.getCard();
        //get value of hidden card
        value = cardValue(card);
        //set value of hidden card
        dealer.setFirstCard(value);
        //add value to dealer's score
        dealer.setScore(value);

        //get dealer's second card
        card = deck.getCard();
        value = cardValue(card);
        dealer.setScore(value);
        view.displayCard(card, 1, dealer.getVisibleScore());

        //get player's cards
        card = deck.getCard();
        //set value of player's card
        value = cardValue(card);
        //add value to player's score
        player.setScore(value);
        //display the card
        view.displayCard(card, 2, player.getScore());

        //get player's second dealt card
        card = deck.getCard();
        value = cardValue(card);
        player.setScore(value);
        view.displayCard(card, 2, player.getScore());

        //display the bet amount
        view.displayBet(player.setBet(), player.getMoney(0));

    }

    //called when player presses hit button
    public void dealerHit(){
        card = deck.getCard();
        value = cardValue(card);
        dealer.setScore(value);
        view.displayCard(card, 1, dealer.getVisibleScore());
        checkScore();
    }

    //also called when player presses hit button
    public void playerHit(){
        card = deck.getCard();
        value = cardValue(card);
        player.setScore(value);
        view.displayCard(card, 2, player.getScore());
        checkScore();
    }

    //checks if both players are over 21
    public void checkScore(){
        if(dealer.getVisibleScore() > 21 && player.getScore() > 21){
            view.busts(player.getScore(), dealer.getScore(), player.getMoney(0));
        }

        if (player.getScore() > 21){
            view.dealerWins(player.getScore(), dealer.getScore(), player.getMoney(2));
        }

        if(dealer.getVisibleScore() > 21){
            view.playerWins(player.getScore(), dealer.getScore(), player.getMoney(1));
        }


    }

    public int getDealerScore(){
        return dealer.getScore();
    }

    //called when player presses stick button
    public void reportScore(){
        //checks whether both players are over
        checkScore();
        //otherwise, check if dealer only is over
        if (dealer.getScore() > 21) {
            player.getMoney(1);
            //if so, player wins
            view.playerWins(player.getScore(), dealer.getScore(), player.getMoney(1));
        }
        //check is player is over
        else if (player.getScore() > 21) {
            //if so, dealer wins
            view.dealerWins(player.getScore(), dealer.getScore(), player.getMoney(2));
        }
        //if no one is over, check who has highest score
        else {
            if (player.getScore() > dealer.getScore()) {
                view.playerWins(player.getScore(), dealer.getScore(), player.getMoney(1));
            } else if (dealer.getScore() > player.getScore()) {
                view.dealerWins(player.getScore(), dealer.getScore(), player.getMoney(2));
            } else if (dealer.getScore() == player.getScore()) {
                view.tie(player.getScore(), dealer.getScore(), player.getMoney(0));
            }
        }
    }

    //when double down button is clicked, double the bet
    public void doubleBet(){
        player.doubleBet();
        view.displayBet(player.getBet(), player.getMoney(0));
    }

    //when new game button is clicked, reset everything except the player's accumulated money
    public void reset(){
        player.resetScore();
        player.resetBet();
        dealer.resetScore();
        view.reset(player.getMoney(0));
        view.viewSetup();
    }

    //helper method to get card value from file name of card
    public int cardValue(String s){
        int n = s.charAt(0);

        if(n == 'j' || n == 'q' || n == 'k' || n == '1'){
            n = 10;
        }
        else if (n == 'a'){
            n = 1;
        }
        else if (n == '2'){
            n = 2;
        }
        else if (n == '3'){
            n = 3;
        }
        else if (n == '4'){
            n = 4;
        }
        else if (n == '5'){
            n = 5;
        }
        else if (n == '6'){
            n = 6;
        }
        else if (n == '7'){
            n = 7;
        }
        else if (n == '8'){
            n = 8;
        }
        else if (n == '9'){
            n = 9;
        }

        return (int) n;
    }


}
