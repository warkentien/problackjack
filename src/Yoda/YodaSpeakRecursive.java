package Yoda;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by sethf_000 on 11/13/2015.
 *
 * This program uses recursion to output the reverse of a sentence.
 * The user is asked to enter a sentence. The words of the sentence are
 * individually stored in an arrayList. A method is then recursively called
 * on the list, removing and printing the last word in the list each time.
 * This effectively prints the sentence backward.
 */
public class YodaSpeakRecursive {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        //get the sentence from the user
        System.out.print("Enter a sentence: ");
        String string = scan.nextLine();
        System.out.println();

        //create arrayList to hold the words that were entered
        ArrayList<String> words = new ArrayList();

        //split the sentence into individual words and store in arrayList
        for (String word: string.split(" ")){
            words.add(word);
        }

        System.out.print("YodaSpeakRecursive: ");
        //call the recursive method to output the correct result
        yoda(words);


    }

    //method to recursively remove and print the last word in the list
    private static void yoda(ArrayList<String> words) {
        //the recursion terminates when the list is no longer greater than zero
        if (words.size() > 0){
            //remove the last word in the list and store it in variable
            String word = words.remove(words.size() - 1);
            //print the word
            System.out.print(word + " ");
            //recursively call the method with the new smaller list
            yoda(words);
        }

    }

}
