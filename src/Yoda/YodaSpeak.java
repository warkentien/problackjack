package Yoda;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by sethf_000 on 11/13/2015.
 *
 * This program uses iteration to output the reverse of a sentence.
 * The user is asked to enter a sentence. The words of the sentence are
 * individually stored in an arrayList. The list is then iterated over
 * and the words are printed out in reverse order.
 */
public class YodaSpeak {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        //get the sentence from the user
        System.out.print("Enter a sentence: ");
        String string = scan.nextLine();
        System.out.println();

        //create arrayList to hold the words that were entered
        ArrayList<String> words = new ArrayList();

        //split the sentence into individual words and store in arrayList
        for (String word: string.split(" ")){
            words.add(word);
        }

        //get the length of the arrayList
        int length = words.size();

        System.out.print("YodaSpeak: ");
        //if list has elements in it
        if (length > 0){
            //iterate over the list and print the words in reverse order
            for (int i = length - 1; i >= 0 ; i--) {
                System.out.print(words.get(i) + " ");
            }
        }
        //if nothing was entered
        else {
            System.out.println("No entry.");
        }


    }
}


/*
YodaSpeak : Create a console program that takes a sentence and reverses the order
of the words. For example, if the sentence is "The force is strong with you", The output
would be "you with strong is force The". Implement this program iteratively.
 */